# Kong / Konga with Oauth2


## Requirements

- [**docker**](https://docs.docker.com/install/)
- [**docker-compose**](https://docs.docker.com/compose/overview/)
- [**jq**](https://stedolan.github.io/jq/)

## 1. Create the image of Kong

[kong-oidc](https://github.com/nokia/kong-oidc) is a kong plugin that allows you to implement OpenID Connect RP (Relying Party).

### 1.2 Construction of the docker image

We will just have to give the command:

```bash
docker-compose build kong
```

and wait for the image to build.

## 2. Kong DB + Database Migrations

Kong uses a database server (postgresql in our case). For this reason it is necessary to initialize the database by
launching the necessary migrations.

First we start the kong-db service:

```bash
docker-compose up -d kong-db
```

Let's launch kong migrations:

```bash
docker-compose run --rm kong kong migrations bootstrap
```

```bash
docker-compose run --rm kong kong migrations up
```

At this point we can start kong:

```bash
docker-compose up -d kong
```

Let's verify that you have the two services running:

```bash
docker-compose ps
```

And finally, let's verify that the OIDC plugin is present on Kong:

```bash
curl -s http://localhost:8001 | jq .plugins.available_on_server.oidc
```

The result of this call should be `true`. The presence of the plugin does not indicate that it is
already active.

## 3. Konga

We start konga with the command:

```bash
docker-compose up -d konga
```

Konga is listening on port 1337. Therefore we launch a browser and point to the url
[http://localhost:1337](http://localhost:1337).


## 4. Creation of a service and a route

To test the system, we will use [Mockbin](http://mockbin.org/) (a service that generates endpoints to
test HTTP requests, responses, sockets and APIs).

As a reference, please refer to [Kong's Admin API](https://docs.konghq.com/1.3.x/admin-api).

```bash
$ curl -s -X POST http://localhost:8001/services \
    -d name=cotizador-service \
    -d url=<url_service> \
    | python -mjson.tool

```

Make a note of your service id  and use it
to make the next call to kong's api that allows you to add a route to the service.

```bash
$ curl -s -X POST http://localhost:8001/services/<service_id>/routes -d "paths[]=/cotizaciones" \
    | python -mjson.tool
```

We verify that everything works:

```bash
$ curl -s http://localhost:8000/cotizaciones

```

## 5. Enabled this service to Oauth2

```bash
$ curl -X POST localhost:8001/services/cotizador-service/plugins \
    --data "name=oauth2"  \
    --data "config.scopes=["read", "write"]" \
    --data "config.mandatory_scope=true" \
    --data "config.enable_password_grant=true" \
    --data "config.accept_http_if_already_terminated=true" \
    --data "config.token_expiration=180" \
    --data "config.global_credentials=true"
```

## 6. Create a consumer

After enable plugin to specific service on this case service customers, now time to add a consumer who can access the API. 
You need to associate a credential to an existing Consumer object.

```bash
$ curl -X POST http://localhost:8001/consumers/ \
    --data "username=german" \
    --data "custom_id=1"
```

## 7. Create a Application

You need to associate a credential to an existing Consumer object. To create a Consumer, 
you can execute the following request:

```bash
$ curl -X POST http://localhost:8001/consumers/{consumer_id}/oauth2 \
    --data "name=Test%20Application" \
    --data "client_id=client1" \
    --data "client_secret=client1Secret" \
    --data "redirect_uris=http://localhost:8000/cotizaciones"
```

## 8. Call service "cotizador-service"

```bash
$ curl -X GET http://localhost:8000/cotizaciones \
```
The method responsed: "The access token is missing" we could not access the API without Authorization 


## 8. Get token Authentication

We need to get valid token for call the previous services

```bash
$ curl -X GET http://localhost:8000/cotizaciones \
```
The method responsed: "The access token is missing" we could not access the API without Authorization 

```bash
curl -X POST \
  --url "https://127.0.0.1:8443/cotizaciones/oauth2/token" \
  --data "client_id=client1" \
  --data "client_secret=client1Secret" \
  --data "grant_type=password" \
  --data "provision_key=<provision_id>" \
  --data "authenticated_userid=german" \
  --data "scope=read" \
  --insecure
```

## 8. Call the service with access token

```bash
curl -X POST \
  --url "http://localhost:8000/cotizaciones" \
  --header "Authorization: Basic <Token>" \
```